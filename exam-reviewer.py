#!/bin/env python3
# This script is used to compare two files, used to review student exam answers against correct answers
# Example usage: python3 exam-reviewer.py -e ./example/chapter1.txt -c ./example/chapter1-answers.txt
#   where -e is student exam file and -c is correct answers file
import argparse
import os.path

APP_NAME = '.: Exam-Reviewer :.'
APP_DESC = 'This script is used to check corrects answers between to files: exam and correct answers file.'

class ExamReviewer:
  exam_file = []
  answers_file = []
  incorret_answers = []
  correct_answers = []

  def __init__(self, args):
    self.args = args

  def run(self):
    print("Running {}".format(APP_NAME))
    self.read_files()
    self.review_files()
    self.print_results()

  def open_file(self, file_name, dest):
    if not os.path.isfile(file_name):
      self.go_exit(msg="{} File doesn't exist, please verify path for this file.".format(file_name))
    with open(file_name) as f:
      for line in f:
        if not line:
          continue
        dest.append(line.strip('\n'))

  def go_exit(self, code=1, msg='Script fail'):
    print(msg)
    exit(code)

  def read_files(self):
    self.open_file(self.args.exam_file, self.exam_file)
    self.open_file(self.args.answers_file, self.answers_file)
  
  def review_files(self):
    if len(self.answers_file) != len(self.exam_file):
      self.go_exit(1, "Not same number or lines in {} and {}".format(self.args.exam_file, self.args.answers_file))
    for index, value in enumerate(self.answers_file):
      if self.exam_file[index] == self.answers_file[index]:
        self.correct_answers.append([index + 1, value])
      else:
        self.incorret_answers.append([index + 1, value])

  def print_results(self):
    print("--- Results of {} reviewing {} ---".format(self.args.exam_file, self.args.answers_file))
    print("Correct answers:") 
    for correct in self.correct_answers:
      print(correct)
    print("--------------------")
    print("Incorrect answers:")
    for incorrect in self.incorret_answers:
      print(incorrect)
    print("--------------------")
    print("\n{} \n [ RESULTS: {} / {} of correct answers. ]\n".format(self.args.exam_file ,len(self.correct_answers), len(self.exam_file)))  
    print("--------------------")

if __name__ == '__main__':
  parser = argparse.ArgumentParser(
    prog=APP_NAME, description=APP_DESC)
  parser.add_argument('-e', '--exam',  dest='exam_file', required=True, help='Exam to validate')
  parser.add_argument('-c', '--correct-answers', dest='answers_file', required=True, help='Correct answers file')

  args = parser.parse_args()
  script = ExamReviewer(args)
  script.run()