# Exam-Reviewer
![Exam-Reviewer Example](./example.jpg)

BG from: https://wallhaven.cc/w/5wyke3

This script is used to compare two files, used to review student exam answers against correct answers. 

# Format for both files
Student's exam and correct answers file are formated by new line answers separation) see path '/examples' txt files in this repo

# Example usage: 
```bash
python3 exam-reviewer.py -e ./example/chapter1.txt -c ./example/chapter1-answers.txt
```
where -e option is 'student exam file' and -c is 'correct answers file'

Simple, That's it, Enjoy it :)